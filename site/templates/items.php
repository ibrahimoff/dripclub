<?php namespace ProcessWire;

$children = null;
if($page->hasChildren("template=items")){
    $children = $pages->find('template=item,has_parent='.$page->id);
}else{
    $children =  $page->children();
}

require('./inc/header.php'); ?>

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-header"><span><?php echo $page->title ?></span></div>
                    <ol class="breadcrumb">
                        <?php foreach ($page->parents() as $item): ?>
                            <?php if (!$item->hasChildren("template=items")) { ?>
                                <li><?php echo $item->title; ?></li>
                            <?php } else { ?>
                                <li><a href='<?php echo $item->url; ?>'><?php echo $item->title; ?></a></li>
                            <?php }; ?>
                        <?php endforeach; ?>
                        <li class="active"><?php echo $page->title; ?></li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="">

                    <?php
                    $curr = $pages->get('/site-settings/')->currency;
                    $results = $page->children('limit=' . $page->item_limit);
                    $pagination = $results->renderPager(array(
                        'nextItemLabel' => false,
                        'previousItemLabel' => false,
                        'currentItemClass' => 'active',
                        'listMarkup' => "<div class='col-lg-12 text-center'><ul class='pagination'>{out}</ul></div>",
                        'itemMarkup' => "<li class='{class}'>{out}</li>",
                        'linkMarkup' => "<a href='{url}'>{out}</a>",
                    ));
                    foreach ($children as $value) :?>
                        <div class="col-sm-3">
                            <?php echo renderItemThumb($value, $curr) ?>
                        </div>

                        <?php
                    endforeach; ?>

                </div>
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
<?php require('./inc/footer.php'); ?>