/*global $*/
(function (window) {
    var map;
    var marker;
    function isDesktop(){
        return $(window).width() > 991;
    }
    $(window).resize(function () {
        menuHoverable();
        submenuClick();
        onDropdownClick();

    });
    $('.items-category').on('hidden.bs.dropdown', function () {
        onDropdownClick();
    })
    function menuHoverable(){
        var navDropDown = $(".navbar .dropdown.items-category");
        var toggleLink = $(".navbar .dropdown.items-category > a");
        var navDropDownSubmenu = $(".dropdown-submenu");
        if(isDesktop()){
            navDropDown.addClass("hoverable-menu");
            toggleLink.attr('data-toggle','');
            toggleLink.removeClass("dropdown-toggle");
            navDropDownSubmenu.removeClass("clickable-menu").addClass("hoverable-menu");
        }else{
            navDropDown.removeClass("hoverable-menu")
            toggleLink.attr('data-toggle','dropdown');
            toggleLink.addClass("dropdown-toggle");
            navDropDownSubmenu.removeClass("hoverable-menu").addClass("clickable-menu");

        }
    }

    function submenuClick(){
        function action(e){
            e.stopPropagation();
            $(this).toggleClass('open')
        }
        $(document).on("click",'.clickable-menu',action)
    }

    function onMouseEnterLeave() {
        function action(e) {

            if (e.type == 'mouseenter') {
                $(this).addClass('open');
            } else {
                $(this).removeClass('open');
            }
        }

        $(document).on({mouseenter: action, mouseleave: action}, '.navbar .dropdown.items-category.hoverable-menu');
    }

    function onItemThumbClick() {
        function action() {
            var link = $(this).find('a');
            window.location = link.attr('href');
        }

        $(document).on("click", '.item-thumb', action);
    }

    function onDailySpecClick() {
        function action() {
            var href = $(this).data('href');
            window.open(href, '_blank');
        }

        $(document).on('click', '.daily_spec', action);
    }

    function itemsPerPage() {
        function action() {
            var select = $(this);
            var url = window.location.href.toString();
            if (url.lastIndexOf('?limit') != -1) {
                url = url.substring(url.lastIndexOf('?limit'), -1);
            }
            window.location.replace(url + '?' + select.val());
        }

        $(document).on('change', '#items_per_page', action);
    }

    function initMap() {
        if (document.getElementById('map')) {
            var mapElement = document.getElementById('map');
            var latLng = {lat: Number(mapElement.dataset.lat), lng: Number(mapElement.dataset.lng)};
            map = new google.maps.Map(mapElement, {
                center: latLng,
                zoom: Number(mapElement.dataset.zoom)
            });
            marker = new google.maps.Marker({
                position: latLng,
                map: map,
                title: mapElement.dataset.addr
            });
        }
    }

    function initSliders() {
        $(".main-slider").owlCarousel({
            slideSpeed: 300,
            singleItem: true,
            pagination: false,
            rewindSpeed: 500
        });
        $(".owl-item-carousel").owlCarousel({
            margin: 15,
            autoplay: true,
            slideBy: 3,
            loop: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });
        $(".top-slider").owlCarousel({
            autoplay: true,
            loop: true,
            slideSpeed : 10000,
            paginationSpeed : 10000,
            items : 1,
            itemsDesktop : false,
            itemsDesktopSmall : false,
            itemsTablet: false,
            itemsMobile : false
        });

        // $(".category-slider").owlCarousel({
        //     autoplay: true,
        //     dots:true,
        //     loop: true,
        //     margin: 15,
        //     slideSpeed : 10000,
        //     paginationSpeed : 10000,
        //     items : 1,
        //     itemsDesktop : false,
        //     itemsDesktopSmall : false,
        //     itemsTablet: false,
        //     itemsMobile : false
        // });
        $(".category-slider").owlCarousel({
            margin:15,
            slideSpeed : 300,
            paginationSpeed : 400,
            items : 1,
            // nav:true,
            autoplay:true,
            itemsDesktop : false,
            itemsDesktopSmall : false,
            itemsTablet: false,
            itemsMobile : false

        });
    }

    function getHeightSliderParent(){
        var navOffset = $(".navbar").height();
        var windowHeight = $(window).height();
        var parentHegiht = $(".top-slider .item");
        var resultHeight = windowHeight - navOffset;
        parentHegiht.css('height',resultHeight);
    }
    function onDropdownClick(){
        $(".clickable-menu").removeClass('open')
        $(".dropdown").removeClass('open')
    }
    getHeightSliderParent();
    onItemThumbClick();
    onMouseEnterLeave();
    itemsPerPage();
    onDailySpecClick();
    // initMap();
    initSliders();
    menuHoverable();
    submenuClick();


    const backgroundVideo = new BackgroundVideo('.bv-video', {
        src: [document.getElementsByClassName('bv-video')[0].dataset.link
        ],
        onReady: function () {
            $(".bv-video-wrap").css({})
        }
    });
})(window)