<?php namespace ProcessWire;
require('./inc/header.php'); ?>
<?php /** @var Object $page */
$res = $page->image_slider;
if ($res->count() > 0):?>
    <div class="slider-holder hidden-xs hidden-sm">
        <div class="top-slider">
            <?php foreach ($res as $item): ?>
                <div class="item" style="background: url('<?php echo $item->url ?>')"></div>

            <?php endforeach; ?>
        </div>

    </div>
<?php else: ?>
    <div class="hidden-xs hidden-sm">
        <video class="bv-video" muted autoplay loop  data-link="<?php echo $page->main_video->url?>"></video>

    </div>
<!--    <section class="hero hidden-xs">-->
<!--        <video id="home-background" autoplay muted loop>-->
<!--            <source src="--><?php //echo $page->main_video->url ?><!--" type="video/mp4">-->
<!--            Your browser does not support the video tag.-->
<!--        </video>-->
<!--    </section>-->
<?php endif; ?>

    <!--<main>-->
    <!--  <video muted class="bv-video" data-link="<? //php echo $page->main_video->url
    ?>"></video>-->
    <!--</main>-->

    <div class="content">
        <div class="container">
            <div class="daily-special-block">
                <div class="section-header"><span><?php echo 'Daily special' ?></span></div>

                <?php $results = $pages->find('template=post,limit=1,sort=-id');
                if ($results):
                    foreach ($results as $value):?>
                        <div class="well daily_spec card-2" data-href="<?php echo $value->link ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="img-responsive" src="<?php echo $value->picture->url ?>" alt="">
                                </div>
                                <div class="col-md-6">
                                    <div class="well-body">
                                        <h4 class="media-heading"><?php echo $value->title ?></h4>
                                        <p><?php echo $value->body ?></p>
                                        <div class="read_more pull-left"><a href="<?php echo $value->link ?>">Read
                                                more</a></div>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>

                    <?php endforeach;endif; ?>
            </div>
            <div class="row">
                <?php
                $curr = $pages->get('/site-settings/')->currency;
                foreach ($page->sliders as $slider):?>
                    <!--                <div class="section-header"><span>--><?php //echo $slider->headline ?><!--</span></div>-->
                    <div class="col-md-4">
                        <div class="main-carousel category-slider">
                            <?php foreach ($slider->selected_items as $value): ?>
                                <?php echo renderItemThumb($value, $curr) ?>
                            <?php endforeach; ?>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>



        </div>
    </div>
    <div class="content">
        <!--<div class="section-bgr" style="background:url(/site/templates/test.jpg);"></div>-->
        <div class="parallax-window hidden-xs hidden-sm" data-parallax="scroll"
             data-image-src="<?php echo $page->parallax_image->url ?>"></div>
        <div class="black-overlay"></div>
        <div class="container contacts-section">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="address-block">
                        <div class="sh1">CONTACTS</div>
                        <ul class="contact-info-list">
                            <li class="sh2"><span class="label-icon"><i class="fa fa-phone"
                                                                        aria-hidden="true"></i></span>
                                <span class="list-value">
                                    <?php foreach ($page->phones as $item): ?>
                                        <?php echo $item->phone ?>;
                                    <?php endforeach; ?>
                                </span>
                            </li>
                            <li class="sh2"><span class="label-icon"><i class="fa fa-map-marker"
                                                                        style="font-size: 16px;" aria-hidden="true"></i></span>
                                <span class="list-value"><?php echo $page->google_map->address ?></span>
                            </li>
                            <li class="sh2"><span class="label-icon"><i class="fa fa-at" aria-hidden="true"></i></span>
                                <span class="list-value"><?php foreach ($page->emails as $item): ?>
                                        <?php echo $item->email ?>
                                    <?php endforeach; ?></span>
                            </li>
                        </ul>
                        <div class="sh1">GET IN TOUCH</div>
                        <ul class="social-pages-list">
                            <?php foreach ($page->social_pages as $item): ?>
                                <li><a href="<?php echo $item->link ?>"><i class="<?php echo $item->icon ?>"></i></a>
                                </li>
                            <?php endforeach; ?>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">


                    <form class="form-horizontal no-margin-form-group">

                            <!-- Text input-->

                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>First name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>Last name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>Subject</label>
                                    </div>
                                </div>
                            </div>
                            <!-- Textarea -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="group">
                                        <textarea id="textarea" name="message" required></textarea>
                                        <span class="bar"></span>
                                        <label>Message</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button class="btn btn-block" type="submit" style="text-transform: uppercase"><span>Send message</span>
                                    </button>
                                </div>
                            </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
    <?php $map = $modules->get('MarkupGoogleMap');
        echo $map->render($page, 'google_map',['type'=>'ROADMAP']);
    ?>
<!--    <div id="map" style="height:300px;" data-addr="--><?php //echo $page->google_map->address ?><!--"-->
<!--         data-lat="--><?php //echo $page->google_map->lat ?><!--" data-lng="--><?php //echo $page->google_map->lng ?><!--"-->
<!--         data-zoom="--><?php //echo $page->google_map->zoom ?><!--"></div>-->
<?php require('./inc/footer.php'); ?>