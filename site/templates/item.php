<?php namespace ProcessWire;
require('./inc/header.php'); ?>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-header"><span><?php echo $page->title ?></span></div>
                    <ol class="breadcrumb">
                        <?php
                        $curr = $pages->get('/site-settings/')->currency;
                        foreach ($page->parents()->not('template=hiddentemplate') as $item):?>
                            <?php if ($item->hasChildren("template=items") || $item->parents->count != 0) { ?>
                                <li><a href='<?php echo $item->url; ?>'><?php echo $item->title; ?></a></li>
                            <?php } else { ?>
                                <li><?php echo $item->title; ?></li>
                            <?php }; ?>
                        <?php endforeach; ?>
                        <li class="active"><?php echo $page->title; ?></li>
                    </ol>
                </div>
            </div>


            <div class="well card-2 item-well">
                <div class="">
                    <div class="col-lg-6">
                        <div class="fotorama" data-nav="thumbs" data-width="600" data-maxwidth="100%"
                             data-allowfullscreen="true">
                            <?php foreach ($page->images as $image) : ?>
                                <img class="" src="<?php echo $image->url ?>" alt="<?php echo $image->description ?>">
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h4 class="pull-right"><?php echo $page->discount ? '<span>' . $page->discount . '<span style="text-decoration:line-through"><sup>' . $page->price . '</sup></span></span>' : $page->price; ?>
                            <span><?php echo $curr; ?></span></h4>
                        <h4><?php echo $page->title ?></h4>
                        <?php echo $page->body; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header"><span><?php echo 'Other items' ?></span></div>

                    <div class="owl-item-carousel">
                        <?php foreach ($page->parent->children()->not('id=' . $page->id) as $value): ?>
                            <?php echo renderItemThumb($value, $curr) ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>


    </div>

    </div>


<?php require('./inc/footer.php'); ?>