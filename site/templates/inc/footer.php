<footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>


    <script src="<?php echo AIOM::JS("libs/jquery/jquery-1.9.1.js")?>"></script>
    <script src="<?php echo AIOM::JS("libs/bootstrap-3.3.7-dist/js/bootstrap.min.js")?>"></script>
    <script src="<?php echo AIOM::JS("libs/owl-carousel/owl.carousel.js")?>"></script>
    <script src="<?php echo AIOM::JS("libs/fotorama-4.6.4/fotorama.js")?>"></script>
    <script src="<?php echo AIOM::JS("libs/parallax.js-1.4.2/parallax.js")?>"></script>
    <script src="<?php echo AIOM::JS("scripts/fastclick.js")?>"></script>
    <script src="<?php echo AIOM::JS("libs/backgroundVideo/dist/backgroundVideo.js")?>"></script>
    <script src="<?php echo AIOM::JS("scripts/main.js")?>"></script>
</body>
</html>
