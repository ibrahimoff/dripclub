<?php namespace ProcessWire;
require('./inc/header.php');?>
<div class="container">
    <div class="content text-center">
        <h1><?php echo $page->title?></h1>
        <div><?php echo $page->body?></div>
    </div>

</div>

<?php require('./inc/footer.php');?>