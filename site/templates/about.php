<?php namespace ProcessWire;
require('./inc/header.php');?>
<div class="content"> 
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-header"><span><?php echo $page->title?></span></div>
            </div>
            <div class="col-lg-12 card-2"><?php echo $page->body?></div>
        </div>
       
    </div>    
</div>        


<?php require('./inc/footer.php');?>